<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJogjaBersihsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jogja_bersihs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_twitter');
            $table->text('text_twitter');
            $table->string('id_user');
            $table->string('user_name');
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jogja_bersihs');
    }
}
