@extends('welcome')

@section('header', ' Jogja Bersih')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

            <!--begin:: Widgets/Trends-->
            <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Top 10 Popular Word
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body pt-0">
                    @php
                        $ctp = 1;
                    @endphp
                    @foreach ($result_data['top_10'] as $item)
                        <h5>{{$ctp++}}. {{$item['text']}}</h5> <br>
                    @endforeach
                </div>
            </div>

            <!--end:: Widgets/Trends-->
        </div>
        <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

            <!--begin:: Widgets/Sales Stats-->
            <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Word Cloud of Tweet
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="chartdiv" style="height: 100%"></div>
                </div>
            </div>

            <!--end:: Widgets/Sales Stats-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

            <!--begin:: Widgets/Trends-->
            <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                {{-- <div class="kt-portlet__head kt-portlet__head--noborder">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Sentimen Analisis
                        </h3>
                    </div>
                </div> --}}
                <div class="kt-portlet__body">
                    <canvas id="sentimen-chart" width="800" height="450"></canvas>
                </div>
            </div>

            <!--end:: Widgets/Trends-->
        </div>
        <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">

            <!--begin:: Widgets/Sales Stats-->
            <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
                <div class="kt-portlet__body">
                    <canvas id="pie-chart" width="800" height="450"></canvas>
                </div>
            </div>

            <!--end:: Widgets/Sales Stats-->
        </div>
    </div>

    <div class="col-lg-12 col-xl-12 order-lg-1 order-xl-1">
        <div class="kt-portlet kt-portlet--head--noborder kt-portlet--height-fluid">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data of Lastest Tweet
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <table id="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Tweet</th>
                            <th>Sentimen</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($result_data['table'] as $item)
                            <tr>
                                <td>{{$item['user_name']}}</td>
                                <td>{{$item['text_twitter']}}</td>
                                <td>
                                    @if ($item['sentimen'] == "positive")
                                        <span class="badge badge-success">{{$item['sentimen']}}</span>
                                    @elseif($item['sentimen'] == "negative")
                                        <span class="badge badge-danger">{{$item['sentimen']}}</span>
                                    @else
                                        <span class="badge badge-secondary">{{$item['sentimen']}}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.4.0/dist/chartjs-plugin-datalabels.min.js"></script>
    <script>
        am4core.ready(function() {
        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chartdiv", am4plugins_wordCloud.WordCloud);
        var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());

        series.accuracy = 4;
        series.step = 15;
        series.rotationThreshold = 0.7;
        series.maxCount = 200;
        series.minWordLength = 2;
        series.labels.template.tooltipText = "{word}: {value}";
        series.fontFamily = "Courier New";
        series.maxFontSize = am4core.percent(30);

        series.text = "{!! $result_data['wc']!!}";

        });
    </script>
    <script>
        new Chart(document.getElementById("sentimen-chart"), {
            type: 'bar',
            data: {
            labels: ["Positive", "Neutral", "Negative"],
            datasets: [
                {
                label: "Sentence Sentimen",
                backgroundColor: ["#aff7a1", "#fad649","#e18582"],
                data: [{{ $result_data['positive'] }},{{ $result_data['neutral'] }},{{ $result_data['negative'] }}]
                }
            ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Last 7 days Sentence Sentimen'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
    <script>
        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: [
                    @foreach ($result_data['top_10'] as $item)
                        "{{$item['text']}}",
                    @endforeach
                ],
                datasets: [{
                    label: "Popular Word",
                    backgroundColor: ["#e74d44", "#f07067","#ed733a","#f39362","#f1a040", "#f7b863", "#f4bb45", "#f8de4e", "#b8d948", "#e0ef85"],
                    data: [
                        @foreach ($result_data['top_10'] as $item)
                           {{$item['value']}},
                        @endforeach
                    ]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Last 7 days Popular Words'
                },
                tooltips: {
                    enabled: false
                },
                plugins: {
                    datalabels: {
                    formatter: (value, ctx) => {
                        let datasets = ctx.chart.data.datasets;
                        if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                        let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                        let percentage = Math.round((value / sum) * 100) + '%';
                        return percentage;
                        } else {
                        return percentage;
                        }
                    },
                    color: '#fff',
                    }
                }
            },
        });

    </script>
@endsection
