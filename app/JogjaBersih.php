<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $id_twitter
 * @property string $text_twitter
 * @property string $id_user
 * @property string $user_name
 * @property string $location
 * @property string $created_at
 * @property string $updated_at
 */
class JogjaBersih extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id_twitter', 'text_twitter', 'id_user', 'user_name', 'location', 'sentimen', 'created_at', 'updated_at'];

}
