<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twitter;

use App\JogjaBersih;
// use SentimentAnalysis;
use Antoineaugusti\LaravelSentimentAnalysis\SentimentAnalysis;

class JogjaBersihController extends Controller
{
    public function __construct(){
        $this->sentiment = new SentimentAnalysis();
    }
    public function cron(){
        $get_data = json_decode(Twitter::getSearch(['q' => "jogjabersih", 'format' => 'json']));

        if ($get_data->statuses != []) {
            // $sentiment = new SentimentAnalysis();
            // dd(storage_path('app/public/sentimen/'));
            // dd($analysis);
            foreach ($get_data->statuses as $key => $value) {
                // $data[$key] = $this->sentiment->decision('this city is rubbish so many trash everywhere');

                $save = JogjaBersih::updateOrCreate(
                    [
                        'id_twitter' => $value->id_str,
                        'id_user' => $value->user->id_str,
                    ],
                    [
                        'text_twitter' => $value->text,
                        'user_name' => $value->user->name,
                        'location' => $value->coordinates,
                        'sentimen' => $this->sentiment->decision($value->text),
                    ]
                );
            }
        }


        return response()->json($save);
    }

    // untuk word cloud
    public function word(){
        $data       = JogjaBersih::orderBy('id', 'desc')->take(100);
        $only_text  = $data->pluck('text_twitter');
        $data_all   = $data->get();

        $pieces =[];
        foreach ($only_text as $key => $value) {
            $pieces[] = explode(" ", $value);
        }
        $result = call_user_func_array("array_merge", $pieces);

        // validasi hasil
        foreach ($result as $key => $value) {
            if (strpos($value, '#') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, ',') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, '.') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'di') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'so') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'in') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'i') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'the') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'very') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'not') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'of') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'where') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'many') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'to') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'thats') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'should') !== false) {
                unset($result[$key]);
            }
            if (strpos($value, 'sebab') !== false) {
                unset($result[$key]);
            }
        }
        $result = array_values($result);
        asort($result);

        // return $result;
        $count = array_count_values($result);
        arsort($count);


        $wc = [];
        foreach ($count as $key => $value) {
            $wc[] = [
                'text'  => $key,
                'value' => $value,
            ];

        }
        $array_10 = array_slice($wc, 0, 10);

        $negative = 0;
        $neutral = 0;
        $positive = 0;
        foreach ($data_all as $key => $value) {
            if ($value->sentimen == "negative") {
                $negative += 1;
            }else if ($value->sentimen == "positive") {
                $positive += 1;
            }else {
                $neutral += 1;
            }
        }

        $result_data = [
            'table'     => $data_all ?? [],
            'top_10'    => $array_10 ?? [],
            'wc'        => implode(" ", $result) ?? null,
            'negative'    => $negative,
            'neutral'    => $neutral,
            'positive'    => $positive,
        ];

        // return $result_data;
        return view('jogja_bersih', compact('result_data'));
    }
}
